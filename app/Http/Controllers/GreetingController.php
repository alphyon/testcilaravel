<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class GreetingController extends Controller
{
    public function index(Request $request){
     try{
         if($request->message === 'null'){
             throw new \Exception("sin datos");
         }
            return response()->json(['data' => $request->message], 200);
        }catch (\Exception $exception){
            return response()->json(['data' => ''], 404);
        }
    }
}
